-module(mylists).
-export([sum/1]).
-export([map/2]).
-export([filter/2]).
-export([reverse/2]).

sum([H|T]) -> H + sum(T);
sum([]) -> 0.

map(_, []) ->[];
map(F, [H|T]) -> [F(H)|map(F,T)].

%%filter(P, [H|T]) -> filter1(P(H), H, P, T);
%%filter(P, []) ->[].

%%filter1(true, H, P, T) -> [H|filter(P, T)];
%%filter1(false, H, P, T) -> filter(P, T).

filter(P, [H|T]) ->
	case P(H) of
		true -> [H|filter(P, T)];
		false -> filter(P, T)
	end;
filter(P, []) ->
	[].

reverse([H|T], Result) ->
	reverse(T, [H|Result]);
reverse([], Result) ->
	Result.